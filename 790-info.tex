\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{url}
\usepackage{colortbl}

\title{Mathematics for Social and Biological Sciences}
\subtitle{Math 790 and Beyond}
\institute{Duke University}
\author{Brian David Fitzpatrick}
\date{\today}

\titlegraphic{\includegraphics[scale=.175]{dukemath.pdf}}

\begin{document}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}




\section{Overview of Math 790}

\begin{frame}

  \frametitle{\secname}

  Math 790 is for graduate students in the social and biological sciences.

  \begin{itemize}
  \item Entitled \emph{Mathematics for Social and Biological Sciences}.
  \item Sequence of three independent ``modules'' per semester.
  \item Minimal mathematical knowledge assumed.
  \item Goal is to equip students with the habits and vocabulary necessary for
    learning mathematics relevant to their research.
  \item ``Low stakes'' environment.
  \end{itemize}

\end{frame}


\section{Math 790 Instructors}
\subsection{Jack Bookman}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\linewidth]{bookman.jpg}
    \end{figure}
    \column{.5\textwidth}
    \begin{itemize}
    \item Emeritus Professor of the Practice in Mathematics
    \item PhD in Mathematics Education, University of North Carolina at Chapel
      Hill
    \item Instructor at Duke since 1982.
    \item Head of the ``instructor training'' program for mathematics PhD
      students.
    \item \url{bookman@math.duke.edu}
    \end{itemize}
  \end{columns}

\end{frame}



\subsection{Brian Fitzpatrick}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=.9\linewidth]{fitzpatrick.jpg}
    \end{figure}
    \column{.5\textwidth}
    \begin{itemize}
    \item Lecturer in Mathematics
    \item PhD in Mathematics, Duke University (2017)
    \item \url{bfitzpat@math.duke.edu}
    \end{itemize}
  \end{columns}

\end{frame}


\section{Math 790 Fall Courses}
\subsection{790.01: Differential Calculus (Bookman)}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{27-Aug to 24-Sep}
    Covers essential concepts, techniques, and theory necessary for students to
    use calculus in more advanced settings.

    \begin{multicols}{3}
      \begin{itemize}
      \item exponential functions
      \item inverse functions
      \item logarithms
      \item limits
      \item continuity
      \item derivatives
      \item optimization
      \item rates of growth
      \end{itemize}
    \end{multicols}

    Meets MWF 10:05 AM - 11:20 AM in Biological Sciences 155.
  \end{block}


\end{frame}



\subsection{790.02: Discrete Probability (Fitzpatrick)}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{26-Sep to 26-Oct}
    Covers essential concepts, techniques, and theory necessary for students to
    use probability distributions in more advanced settings.

    \begin{multicols}{3}
      \begin{itemize}
      \item conditional probability
      \item Bayes' rule
      \item independence
      \item random variables
      \item distributions
      \item expected value
      \item standard deviation
      \item variance
      \end{itemize}
    \end{multicols}

    Meets MW 10:05 AM - 11:20 AM in Biological Sciences 155.
  \end{block}


\end{frame}



\subsection{790.03: Linear Algebra (Fitzpatrick)}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{29-Oct to 30-Nov}
    Covers essential concepts, techniques, and theory necessary for students to
    use matrices in more advanced settings.

    \begin{multicols}{2}
      \begin{itemize}
      \item matrix algebra
      \item systems of linear equations
      \item projection problems
      \item least squares approximations
      \item eigenvalues and eigenvectors
      \end{itemize}
    \end{multicols}

    Meets MW 10:05 AM - 11:20 AM in Biological Sciences 154.
  \end{block}


\end{frame}


\section{Math 790 Spring Courses}
\subsection{Integral Calculus and Other Topics}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Math 790 will again consist of three modules in the Spring.

  \begin{itemize}
  \item 790.01: Integral Calculus (Bookman)
  \item 790.02: TBA (likely matrix decompositions, Fitzpatrick)
  \item 790.03: TBA (likely multivariable calculus, Fitzpatrick)
  \end{itemize}

\end{frame}


\section{Beyond 790}
\subsection{Options for Grad Students}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Graduate students are often interested in mathematics beyond Math 790.
  \begin{itemize}
  \item Many of the math department's ``undergrad'' courses can be useful.
  \item Recommendation: contact the instructor before signing up.
  \end{itemize}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{table}
    \scriptsize
    \begin{tabular}{lll}
      Course Number & Course Title & Fall 2018?\\
      \hline
      MATH 712 & Multivariable Calculus & Yes\\
      MATH 716 & Linear Algebra and Differential Equations & Yes\\
      \rowcolor{blue!30} MATH 718 & Matrices and Vector Spaces & Yes\\
      MATH 721 & Linear Algebra and Applications & Yes\\
      \rowcolor{blue!30} MATH 730 & Elementary Probability & Yes\\
      MATH 733 & Complex Analysis & Yes\\
      MATH 740 & Advanced Introduction to Probability & Yes\\
      MATH 753 & Ordinary and Partial Differential Equations & Yes\\
      MATH 756 & Elementary Differential Equations & Yes\\
      MATH 757 & Introduction to Linear Programming and Game Theory & No\\
      MATH 701 & Introduction to Abstract Algebra & Yes\\
      MATH 703 & Advanced Linear Algebra & No\\
      MATH 711 & Topology & No\\
      \rowcolor{blue!30} MATH 713 & Topological Data Analysis & Yes\\
      MATH 731 & Advanced Calculus I & Yes\\
      MATH 751S & Nonlinear Ordinary Differential Equations & No\\
      MATH 754 & Introduction to Partial Differential Equations & No\\
      \rowcolor{blue!30} MATH 765 & Introduction to High Dimensional Data Analysis & Yes\\
    \end{tabular}
  \end{table}

\end{frame}

\subsection{Math 718: Matrices and Vector Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  Two sections in Fall 2018.
  \begin{table}
    \scriptsize
    \centering
    \begin{tabular}{llll}
      MATH 718.01 & Bar-On, Rann & MWF 12:00 PM-12:50 PM & Physics 259\\
      MATH 718.02 & Fitzpatrick, Brian & TuTh 06:15 PM-07:30 PM & Physics 235\\
    \end{tabular}
  \end{table}

  Topics include
  \begin{multicols}{2}
    \begin{itemize}
    \item systems of linear equations
    \item matrix factorizations
    \item vector subspaces
    \item orthogonality
    \item least squares problems
    \item eigenvalues
    \item singular value decomposition
    \item principal component analysis
    \item applications to data-driven problems
    \end{itemize}
  \end{multicols}



\end{frame}


\subsection{Math 730: Elementary Probability}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Six sections in Fall 2018.
  \begin{table}
    \centering\scriptsize
    \begin{tabular}{llll}
      MATH 730.01 & Wolpert, Robert & MW 10:05 AM-11:20 AM & Soc/Psych 130\\
      MATH 730.02 & Junge, Matthew & TuTh 01:25 PM-02:40 PM & Physics 130\\
      MATH 730.03 & Staff, Departmental & TuTh 10:05 AM-11:20 AM & Social Sciences 119\\
      MATH 730.04 & Junge, Matthew & TuTh 04:40 PM-05:55 PM & Social Sciences 119\\
      MATH 730.05 & Young, Alex & MWF 08:45 AM-09:35 AM & Social Sciences 119\\
      MATH 730.06 & Staff, Departmental & TuTh 08:30 AM-09:45 AM & Social Sciences 119\\
    \end{tabular}
  \end{table}

  Topics include
  \begin{multicols}{2}
    \begin{itemize}
    \item probability models
    \item random variables
    \item independence
    \item distributions
    \item expectation
    \item central limit theorem
    \end{itemize}
  \end{multicols}

\end{frame}


\subsection{Math 713: Topological Data Analysis}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  One section in Fall 2018.
  \begin{table}
    \centering\scriptsize
    \begin{tabular}{llll}
      MATH 713.01 & Harer, John & TuTh 01:25 PM-02:40 PM & Physics 259
    \end{tabular}
  \end{table}

  This course is devoted to using the techniques of persistent homology to find
  multi-scale topological structure in point cloud data.

\end{frame}



\subsection{Math 765: Introduction to High Dimensional Data Analysis}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  One section in Fall 2018.
  \begin{table}
    \centering\scriptsize
    \begin{tabular}{llll}
      MATH 765.01 & Bendich, Paul & TuTh 08:30 AM-09:45 AM & Gross Hall 318
    \end{tabular}
  \end{table}

  Topics include
  \begin{multicols}{2}
  \begin{itemize}
  \item dimension reduction
  \item randomized compression schemes
  \item linear regression
  \item clustering
  \item supervised learning
  \end{itemize}
\end{multicols}



\end{frame}

\end{document}
